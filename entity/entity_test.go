package entity

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_StringToID(t *testing.T) {
	type test struct {
		desc  string
		id    string
		error bool
	}

	cases := []test{
		{
			desc:  "invalid id",
			id:    "invalid",
			error: true,
		},
		{
			desc:  "valid id",
			id:    NewID().String(),
			error: false,
		},
	}
	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			_, err := StringToID(tc.id)
			if tc.error {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}

}
