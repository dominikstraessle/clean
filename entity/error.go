package entity

import "errors"

//ErrInvalidEntity invalid entity
var ErrInvalidEntity = errors.New("invalid entity")

//ErrNotFound not found
var ErrNotFound = errors.New("not found")
