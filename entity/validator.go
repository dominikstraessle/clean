package entity

import (
	"github.com/go-playground/validator/v10"
	"reflect"
)

type Validator struct {
	Validator *validator.Validate
}

type TagValidations map[string]validator.Func
type StructValidation struct {
	structType     interface{}
	validationFunc validator.StructLevelFunc
}
type StructValidations []StructValidation

func (v *Validator) Validate(i interface{}) error {
	err := v.Validator.Struct(i)
	if err != nil {
		return ErrInvalidEntity
	}
	return nil
}

func NewValidator() *Validator {
	return &Validator{
		Validator: validator.New(),
	}
}

func NewRegisteredValidator(tagValidations TagValidations, structValidations StructValidations) (*Validator, error) {
	v := &Validator{
		Validator: validator.New(),
	}

	if err := v.registerTags(tagValidations); err != nil {
		return nil, err
	}

	v.registerStructs(structValidations)

	return v, nil
}

func (v *Validator) registerTags(validations TagValidations) error {
	for key, value := range validations {
		err := v.Validator.RegisterValidation(key, value)
		if err != nil {
			return err
		}
	}
	return nil
}

func (v *Validator) registerStructs(validations StructValidations) {
	for _, s := range validations {
		v.Validator.RegisterStructValidation(s.validationFunc, s.structType)
	}
}

func ConvertibleTo(thisType interface{}, otherType reflect.Type) bool {
	if otherType.ConvertibleTo(reflect.TypeOf(thisType)) {
		return true
	}
	return false
}
