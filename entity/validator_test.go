package entity

import (
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestNewRegisteredValidator(t *testing.T) {
	type args struct {
		tagValidations    TagValidations
		structValidations StructValidations
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "register validations",
			args: args{
				tagValidations: map[string]validator.Func{
					"isValid": func(fl validator.FieldLevel) bool {
						return true
					},
				},
				structValidations: StructValidations{
					{
						structType:     NewID(),
						validationFunc: func(sl validator.StructLevel) {},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "tag name is empty",
			args: args{
				tagValidations: map[string]validator.Func{
					"": func(fl validator.FieldLevel) bool {
						return true
					},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewRegisteredValidator(tt.args.tagValidations, tt.args.structValidations)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewRegisteredValidator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestNewValidator(t *testing.T) {
	v := NewValidator()
	assert.NotNil(t, v)
}

func TestValidation(t *testing.T) {
	type NameStruct struct {
		Name string `validate:"required"`
	}
	type test struct {
		name    string
		entity  NameStruct
		wantErr bool
	}

	tests := []test{
		{
			name: "valid",
			entity: NameStruct{
				Name: "valid",
			},
			wantErr: false,
		},
		{
			name: "invalid",
			entity: NameStruct{
				Name: "",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := NewValidator()
			err := v.Validate(tt.entity)
			if (err != nil) != tt.wantErr {
				t.Errorf("Validate() error = %v, wantErr %v", err, tt.wantErr)

			}
		})
	}
}

func TestConvertibleTo(t *testing.T) {
	type args struct {
		thisType  interface{}
		otherType reflect.Type
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "is string",
			args: args{
				thisType:  "",
				otherType: reflect.TypeOf(""),
			},
			want: true,
		},
		{
			name: "is no string",
			args: args{
				thisType:  -1,
				otherType: reflect.TypeOf(""),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertibleTo(tt.args.thisType, tt.args.otherType); got != tt.want {
				t.Errorf("ConvertibleTo() = %v, want %v", got, tt.want)
			}
		})
	}
}
