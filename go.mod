module gitlab.com/dominikstraessle/clean

go 1.16

require (
	github.com/go-playground/validator/v10 v10.5.0
	github.com/google/uuid v1.2.0
	github.com/stretchr/testify v1.7.0
)
